let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  return collection;
}

function enqueue(element) {
  //In this funtion you are going to make an algo that will add an element to the array
  // Mimic the function of push method
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove first element in the array
  for (let index = 1; index < collection.length; index++) {
    collection[index - 1] = collection[index];
    collection.length--;
  }
  return collection;
}

function front() {
  // In here, you are going to show the first element
  const [first, ...element] = collection;
  return first;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
  let result = 0;
  while (collection[result]) {
    result++;
  }
  return result;
}

function isEmpty() {
  //it will check whether the function is empty or not
  if (!collection) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
